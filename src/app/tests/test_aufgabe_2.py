import unittest

from src.app.routes.admin import get_dynamic_table
from src.app.routes.user import profile_home_blueprint


class TestOrderTable(unittest.TestCase):

    def test_new_feature(self):
        # test_input NICHT anpassen
        test_input = [
            [1, 12, "B: Test Product"],
            [2, 12, "B: Test Product"],
            [3, 12, "B: Test Product"],
            [4, 1, "A: Test Product"],
            [5, 1, "A: Test Product"],
            [6, 12, "Z: Test Product"]
        ]

        dynamic_table = get_dynamic_table(test_input)

        expected_outcome = [
            ["Order ID", "A: Test Product", "B: Test Product", "Z: Test Product"],
            [1, 2, 0, 0],
            [12, 0, 3, 1],
            ["Total", 2, 3, 1]
        ]

        # Testet, ob expected_outcome gleich dem return Wert der Methode get_dynamic_table ist
        self.assertTrue(expected_outcome == dynamic_table)


if __name__ == '__main__':
    unittest.main()
